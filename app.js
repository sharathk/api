var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
 
app.use(bodyParser.json());

Geners = require('./models/geners.js');
Book = require('./models/book.js');

//connect to mongodb
//var MongoClient = require('mongodb').MongoClient;
//var url = "mongodb://localhost:27017/mydb";
mongoose.connect('mongodb://localhost/bookstore'); 
var db = mongoose.connection;


app.get('/', function(req, res){
    res.send('hello world!');
});

app.get('/api/geners', function(req, res){
    Geners.getGeners(function(err, geners){
        if(err){
            throw err;
        }
        res.json(geners);   
     });
}); 

app.get('/api/books', function(req, res){
    Book.getBooks(function(err, books){
      //  if(err){
        //    throw err;
       // }
        res.json(books);   
     });
});

app.get('/api/books/:_id', function(req, res){
    Book.getBookById(req.params._id, function(err, book){
       // if(err){
         //   throw err;
        //}
        res.json(book);   
     });
}); 

app.post('/api/geners', function(req, res){
    var geners = req.body;
    Geners.addGeners(geners, function(err, geners){
        //if(err){
          //  throw err;
        //} 
        res.json(geners);    
     });
});

app.post('/api/books', function(req, res){
    var book = req.body;
    Book.addbook(book, function(err, book){
        //if(err){
          //throw err; 
        //} 
        res.json(book);    
    });
 }); 

 app.delete('/api/books/:_id', function(req, res){
     var id = req.params.id;
 
    Book.removebook(id, function(err, book){
     
     
        res.json(book);    
    });
 }); 

 app.put('/api/books/:_id', function(req, res){
   var id = req.params._id;
    var book = req.body;
    Book.updateBook(id, book, {}, function(err, book){
    
        res.json(book);    
     });
 });




app.listen(8000);
console.log('running on port 8000...');