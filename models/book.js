var mongoose = require('mongoose');

//scheema
var bookSchema = mongoose.Schema({
    title:{
        type: String,
        required: true
    },
    geners:{
        type: String,
        required: true
    },
     description:{
        type: String,
        required: true
    },
     authour:{
        type: String,
        required: true
    },
     publisher :{
        type: String,
        required: true
    },

    create_date:{
        type: Date,
        default: Date.now
    }
});

var Books = module.exports = mongoose.model('Books',bookSchema);

module.exports.getBooks = function(callback, limit){
    Books.find(callback).limit(limit);
}

module.exports.getBookById = function(id, callback){
    Books.findById(id, callback); 
} 


//add book
module.exports.addbook = function(book, callback){
   Books.create(book, callback);
}


//remove
module.exports.removeBook = function(id, callback){
    var query = {_id: id};
    Books.remove(id, callback); 
} 

//updat

module.exports.updateBook = function(id, book, options, callback){
  var query = {_id: id};
   var update = {
        title: book.title,
        geners: book.geners,
        description: book.description,
        authour: book.authour,
        publisher: book.publisher
    }
    Books.findOneAndUpdate(query, update, options, callback);
}